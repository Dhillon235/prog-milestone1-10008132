﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_22
{
    class Program
    {
        static void Main(string[] args)
        {
            var ab = new Dictionary<string, string>();

            ab.Add("Mango", "Fruit");
            ab.Add("Banana", "Fruit");
            ab.Add("Carrot", "Vegetable");
            ab.Add("Pumpkin", "Vegetable");

              foreach (var x in ab)
            {
                Console.WriteLine($"{x.Key}");
            }

            {
                Console.WriteLine($"There are two fruits and two vegetables");
            }

              foreach (var x in ab)
            {
                Console.WriteLine($"{x.Key} is a {x.Value}");
            }
        }
    }
}
