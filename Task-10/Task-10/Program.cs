﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_10
{
    class Program
    {
        static void Main(string[] args)
        {

            int i = 20;  // future time.
            var a = (i / 4);  //leap year comes after every four years.

            Console.WriteLine("Enter the current year");
            Console.ReadLine();

            Console.WriteLine($"There are {a} leap years in next 20 years ");

        }
    }
}
