﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_23
{
    class Program
    {
        static void Main(string[] args)
        {
            var ab = new Dictionary<string, string>();

            ab.Add("Monday", "weekday");
            ab.Add("Tuesday", "weekday");
            ab.Add("wednesday", "weekday");
            ab.Add("Thursday", "weekday");
            ab.Add("Friday", "weekday");
            ab.Add("Satureday", "weekend");
            ab.Add("Sunday", "weekend");
            
            foreach (var x in ab)
            {
                Console.WriteLine($"{x.key} is a {x.value}");
            }
        }
    }
}
