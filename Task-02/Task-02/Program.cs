﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02
{
    class Program
    {
        static void Main(string[] args)
        {
            var month = " ";
            var day = 0;

            Console.WriteLine("Type the month in which you born");
            month = Console.ReadLine();
            Console.WriteLine("Type the day on which you born");
            day = int.Parse(Console.ReadLine());

            Console.WriteLine($"You are born on {month},{day}");
        }
    }
}
