﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_24
{
    class Program
    {
        static void Main(string[] args)
        {
            var MenuSelection = Console.ReadLine();
           
            switch (MenuSelection)
            {
                case "1":
                    Console.WriteLine("Play Chess");
                    break;

                case "2":
                    Console.WriteLine("Play Minecraft");
                    break;
                default:
                    Console.WriteLine("The selection was invalid");
                    break;
                case "3":
                    Console.WriteLine("Go to main menu");
                    break;
            }
        }
    }
}
