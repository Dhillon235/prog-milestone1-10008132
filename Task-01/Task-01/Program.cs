﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01
{
    class Program
    {
        static void Main(string[] args)
        {
            string name, input;
            int age;

            Console.WriteLine("What is your name");
            name = Console.ReadLine();
            Console.WriteLine("What is your age");
            input = Console.ReadLine();
            age = Convert.ToInt32(input);

            Console.WriteLine("Hello " + name + " you are " + age + " years old.");
            Console.WriteLine("Hello {0} you are {1} years old.", name, age);
            Console.WriteLine($"Hello {name} you are {age} years old.");
        }
    }
}
