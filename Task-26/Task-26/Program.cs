﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_26
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new string[5] { "Red", "Blue", "Yellow", "Green", "Pink" };

            Array.Sort(a);  //to print in alphabetical order
            Array.Reverse(a); //to print in descending order

            Console.WriteLine(string.Join(",", a));
        }
    }
}
